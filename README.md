# Assignator

_Software to calculate most interesting person to event assignment loosely based on the stable marriage algorithm._

##### Table of Contents
* [Introduction](#introduction)
* [Usage](#usage)
* [Method](#method)
* [How to build](#how-to-build)

## Introduction

The Assignator is a software which calculates interesting assignments of participants to events with limited number of participants and conflicting schedule. For this it takes a list of events like this:

    - Biology  (capacity: 2, schedule: 13:00 - 17:00)
    - Chemistry (capacity: 2, schedule: 13:00 - 15:00)
    - Geology (capacity: 2, schedule:  15:00 - 17:00)

and of persons interested in participating those events likes this (the order of the events indicates their priorities):

    - Alice, preferres Chemistry, Geology, Biology
    - Bob, preferres Biology, Geology
    - Claire, preferres Chemistry
    - Doug, preferres Geology, Chemistry
    - Eve, preferres Biology
    
It calculates a match like this:

    Biology (13:00 - 17:00): Bob, Eve
    Chemistry (13:00 - 15:00): Alice, Claire
    Geology (15:00 - 17:00): Doug, Alice
    

## Usage

To use the command line version (the only one at the moment) of the software to find assignments, you need to take these steps:


### Data Preparation

Prepare your data in the line based Assignator data format by using two files. You can use these templates; they also contain directions for the line format:

* [events.txt](src/dist/sample_files/events.txt)
* [attendants.txt](src/dist/sample_files/attendants.txt)

If you have your data in a spreadsheet, you need to export it and adapt it to the format. Copy all data into such two files and place them somewhere on your hard drive, e.g. in a directory named `myAssignatorSample/`.

#### Events

The events are to be provided in this format:

    EVENT NAME; EVENT ID; CAPACITY; TIMESLOTS
    
which could for example look like this:

    Chemistry ; chem ; 2 ; 2016-04-02 13:00 - 15:00, 17:00 - 19:00
    Biology   ; bio  ; 2 ; 2016-04-02 13:00 - 17:00
    Geology   ; geo  ; 2 ; 2016-04-02 15:00 - 17:00

Spaces between the delimiters (semicolons) are irrelevant. The EVENT NAME is an arbitrary descriptor only for humans. The EVENT ID is used to refer to the events in the attendants table.

#### Attendants

The attendants are to be provided in this format:

    ATTENDANT NAME; EVENT PREFERENCES
    
which for example could look like this:

     Alice  ; chem, geo, bio
     Bob    ; bio, geo
     Claire ; chem
     Doug   ; geo, chem
     Eve    ; bio

Instead of a preference item you could also provide a number of alternatives:

    Eve    ; bio | chem, geo
    
This reads as: Eve prefers either Biology **or** Chemistry as first priority, and Geology as second. She will be only assigned one of the two alternatives at most.

### Install Java

Then make sure you have the latest Java (JRE) version installed. Get one from [java.com](https://java.com).

### Download Assignator

Download the archive with the latest version from the [tags](https://gitlab.com/nwaldispuehl/assignator/tags) page. Extract it somewhere on your hard drive, e.g. the `myAssignatorSample/`.

### Run Assignator

Now you can run the Assignator from the command line. Assuming your `myAssignatorSample/` directory looks as follows now:

    assignator/
    attendants.txt
    events.txt
    
you can switch to this directory:

    $ cd myAssignatorSample/
    
and then run the program with the two files as arguments:

    $ ./assignator/bin/assignator events.txt attendants.txt

### Results

The results are then printed to the standard out, i.e. the terminal. They are prepared in two ways, once attendant-centric and once from perspective of the events. For our example above they look as follows:

#### Output by attendants

    Match: events: 3, assignees: 5, score: 0.83, attendants:
    
    * Bob, Preferences: bio, geo, hit score: 0.67, events (1):
      - Biology (id: 'bio', capacity: 2) time slot(s): [2016-04-02 13:00 - 17:00]
    
    * Claire, Preferences: chem, hit score: 1, events (1):
      - Chemistry (id: 'chem', capacity: 2) time slot(s): [2016-04-02 13:00 - 15:00, 2016-04-02 17:00 - 19:00]
    
    * Doug, Preferences: geo, chem, hit score: 0.67, events (1):
      - Geology (id: 'geo', capacity: 2) time slot(s): [2016-04-02 15:00 - 17:00]
    
    * Eve, Preferences: bio, hit score: 1, events (1):
      - Biology (id: 'bio', capacity: 2) time slot(s): [2016-04-02 13:00 - 17:00]
    
    * Alice, Preferences: chem, geo, bio, hit score: 0.83, events (2):
      - Chemistry (id: 'chem', capacity: 2) time slot(s): [2016-04-02 13:00 - 15:00, 2016-04-02 17:00 - 19:00]
      - Geology (id: 'geo', capacity: 2) time slot(s): [2016-04-02 15:00 - 17:00]


#### Output by events

    Match: events: 3, assignees: 5, score: 0.83, assignments:
    
    * Biology (id: 'bio', capacity: 2) time slot(s): [2016-04-02 13:00 - 17:00], attendants (2):
      - Bob
      - Eve
    
    * Chemistry (id: 'chem', capacity: 2) time slot(s): [2016-04-02 13:00 - 15:00, 2016-04-02 17:00 - 19:00], attendants (2):
      - Alice
      - Claire
    
    * Geology (id: 'geo', capacity: 2) time slot(s): [2016-04-02 15:00 - 17:00], attendants (2):
      - Alice
      - Doug

#### Scoring

The scoring is a measure of how well the attendants preferences were matched and is calculated per attendant and also in total.

It is in the range of `[0, 1]` and is `0` if not a single assignment was possible and `1` if all preferences of all attendants were matched.
  
##### Per match / in total

The match (or total) score (`score_tot`) is calculated as the sum of every attendants score divided by the number of attendants:

    score_tot := sum( i = 1 ... |attendants|, score_attendant_i ) / |attendants|

###### Example

For Alice (score `0.5`) and Bob (score `0.8`) alone the total score would be `(0.5 + 0.8) / 2 = 0.65`.

##### Per attendant

The attendants score (`score_attendant`) is calculated as the sum of the matched preferences weights. 

    preference_points_p := (|preferences| - p + 1)
    
    preference_weight_p := { 0                                                        if not matched
                           { preference_points_p / sum( i = 1 ... |preferences|, i)   if matched
                                
    score_attendant_a := sum( i = 1 ... |preferences|, preference_weight_i )

###### Example

Alice has three preferences: Chemistry, Geology, Biology. Their respective points:

* Chemistry (1st) `3 - 1 + 1 = 3`
* Geology (2nd) `3 - 2 + 1 = 2`
* Biology (3rd) `3 - 3 + 1 = 1`

yielding a sum of `6`.

Assuming these events are matched their respective preference weight:

* Chemistry (matched) `3 / 6 = 0.5`
* Geology (matched) `2 / 6 = 0.33`
* Biology (not matched) `0`

Alice' attendant score is now the sum of these preference weights:

    0.5 + 0.33 + 0 = 0.833


## Method

Current strategy is the `RandomizedGreedyMatcher`. It performs a number of rounds of which each produces a `Match`. It then chooses the match with the highest score. A round starts with shuffling the attendants list. Every attendant is then asked for his first preference and at the end of the attendants list, it starts over from start now asking the attendants for their next (possible) preference. Attendants always calculate the optimal choice of preferences. The round ends when no attendant has any possible preferences left. 

## How to build

You need Java 8 (JDK) on your machine. Then, after cloning the repository use the provided [gradle](https://gradle.org/) wrapper to:
 
... run the tests:

    $ ./gradlew check

... or build the distribution:

    $ ./gradlew installDist

The application is then to be found in the folder `build/install/assignator` and can be started with:

    $ cd build/install/assignator/bin
    $ ./assignator
