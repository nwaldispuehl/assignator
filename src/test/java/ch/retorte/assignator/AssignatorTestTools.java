package ch.retorte.assignator;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.attendant.Preference;
import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.event.TimeSlot;
import ch.retorte.assignator.match.EventAssignment;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helpers to easy creating fixtures for tests.
 */
public class AssignatorTestTools {

  //---- Fields

  private static DecimalFormat decimalFormat = new DecimalFormat("#.###");


  //---- Methods

  public static <T> List<T> lst(T... items) {
    return Arrays.asList(items);
  }

  public static Event e(String id, int capacity, String... timespans) {
    return new Event(id, capacity, Arrays.stream(timespans).map(AssignatorTestTools::ts).collect(Collectors.toList()).toArray(new TimeSlot[0]));
  }

  public static EventAssignment ea(Event event) {
    return new EventAssignment(event);
  }

  public static TimeSlot ts(String timeSpan) {
    String[] timeSpanParts = timeSpan.split("-");
    return new TimeSlot(hour(timeSpanParts[0].trim()), hour(timeSpanParts[1].trim()));
  }

  static Attendant a(String name, Preference... preferences) {
    return new Attendant(name, preferences);
  }

  static LocalDateTime hour(String hour) {
    return LocalDateTime.parse("2000-01-01T" + hour + ":00:00");
  }

  public static String round(double value) {
    return decimalFormat.format(value);
  }

}
