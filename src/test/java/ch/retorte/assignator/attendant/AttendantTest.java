package ch.retorte.assignator.attendant;

import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.match.EventAssignment;
import ch.retorte.assignator.match.EventAssignmentGroup;
import org.junit.Test;

import java.util.List;

import static ch.retorte.assignator.AssignatorTestTools.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit tests for the {@link Attendant}.
 */
public class AttendantTest {

  private Attendant sut = new Attendant();

  private Event e1 = e("e1", 1, "09 - 12");
  private Event e2 = e("e2", 1, "10 - 11");
  private Event e3 = e("e3", 1, "11 - 13");

  @Test
  public void shouldFindOptimalDistribution() {
    assertDistribution(lst(p(e1)), lst(ea(e1), ea(e2), ea(e3)), e1);
    assertDistribution(lst(p(e1), p(e2), p(e3)), lst(ea(e1), ea(e2), ea(e3)), e1);
    assertDistribution(lst(p(e1), p(e2)), lst(ea(e1)), e1);
  }

  private Preference p(Event event) {
    return new Preference(event);
  }

  private void assertDistribution(List<Preference> preferences, List<EventAssignment> eventAssignments, Event expectedResult) {
    Event highestRankedEvent = Attendant.findHighestRankedEventFor(sut, preferences, new EventAssignmentGroup(eventAssignments));
    assertThat(highestRankedEvent, is(expectedResult));
  }

  @Test
  public void shouldCalculateScore() {
    assertScore(lst(p(e1)), lst(ea(e1).add(sut)), 1.0);
    assertScore(lst(p(e1)), lst(ea(e2).add(sut)), 0);

    assertScore(lst(p(e1), p(e2)), lst(ea(e1).add(sut), ea(e2).add(sut)), 1);
    assertScore(lst(p(e1), p(e2)), lst(ea(e1).add(sut)), 0.667);
    assertScore(lst(p(e1), p(e2)), lst(ea(e2).add(sut)), 0.333);
    assertScore(lst(p(e1), p(e2)), lst(ea(e2)), 0);

    assertScore(lst(p(e1), p(e2), p(e3)), lst(ea(e1).add(sut), ea(e2).add(sut), ea(e3).add(sut)), 1.0);
    assertScore(lst(p(e1), p(e2), p(e3)), lst(ea(e1).add(sut)), 0.5);
    assertScore(lst(p(e1), p(e2), p(e3)), lst(ea(e2).add(sut)), 0.333);
    assertScore(lst(p(e1), p(e2), p(e3)), lst(ea(e3).add(sut)), 0.167);
    assertScore(lst(p(e1), p(e2), p(e3)), lst(ea(e3)), 0);
  }

  private void assertScore(List<Preference> preferences, List<EventAssignment> eventAssignments, double expectedScore) {
    double calculatedScore = sut.calculateScoreWith(sut, preferences, new EventAssignmentGroup(eventAssignments));
    assertThat(round(calculatedScore), is(round(expectedScore)));
  }

  @Test
  public void shouldSortByRank() {
    // given
    Attendant a = new Attendant("a1", p(e2), p(e3), p(e1));

    // when
    List<Event> sortedEvents = a.sortByRank(lst(e1, e2, e3));

    // then
    assertThat(sortedEvents.get(0), is(e2));
    assertThat(sortedEvents.get(1), is(e3));
    assertThat(sortedEvents.get(2), is(e1));
  }



}
