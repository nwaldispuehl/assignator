package ch.retorte.assignator;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.attendant.Preference;
import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.event.TimeSlot;
import ch.retorte.assignator.match.EventAssignment;
import ch.retorte.assignator.match.Match;
import ch.retorte.assignator.matcher.Matcher;
import ch.retorte.assignator.matcher.RandomizedGreedyMatcher;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ch.retorte.assignator.AssignatorTestTools.*;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration test for the {@link RandomizedGreedyMatcher}.
 */
public class RandomizedGreedyMatcherIntegrationTest {

  private Matcher sut = new RandomizedGreedyMatcher();

  @Test
  public void shouldCalculateEmptyMatch() {
    // given
    List<Event> events = new ArrayList<>();
    List<Attendant> attendants = new ArrayList<>();

    // when
    List<Match> results = sut.findMatchesBetween(events, attendants);

    // then
    assertThat(results.size(), is(0));
  }

  @Test
  public void shouldCalculateTrivialMatch() {
    // given
    Event myEvent = new Event("MyEvent", 1, new TimeSlot(hour("09"), hour("11")));
    List<Event> events = asList(myEvent);
    List<Attendant> attendants = asList(new Attendant("Alice", new Preference(myEvent)));

    // when
    List<Match> results = sut.findMatchesBetween(events, attendants);

    // then
    assertThat(results.size(), is(3));
    Match onlyMatch = results.get(0);
    assertThat(onlyMatch.score(), is(1.0));
    assertThat(onlyMatch.getEventAssignmentGroup().getEventAssignments().size(), is(1));
    EventAssignment eventAssignment = onlyMatch.getEventAssignmentGroup().getEventAssignments().iterator().next();
    assertThat(eventAssignment.getEvent().getId(), is("MyEvent"));
    assertThat(eventAssignment.getAttendants().get(0).getName(), is("Alice"));
  }

  @Test
  public void shouldCalculateOptimalMatch() {
    // given
    Event e1 = e("E1", 2, "08 - 10");
    Event e2 = e("E2", 2, "10 - 12");
    Event e3 = e("E3", 2, "09 - 11");

    Attendant a1 = a("A1", new Preference(e1), new Preference(e2));
    Attendant a2 = a("A2", new Preference(e1), new Preference(e3));

    // when
    List<Match> matches = sut.findMatchesBetween(lst(e1, e2, e3), lst(a1, a2));

    // then
    Assert.assertThat(matches.size(), is(6));
    // TODO add better text fixture
  }

 }
