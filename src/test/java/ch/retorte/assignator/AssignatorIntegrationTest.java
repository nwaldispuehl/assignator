package ch.retorte.assignator;

import ch.retorte.assignator.formatter.MatchFormatter;
import ch.retorte.assignator.match.Match;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for the {@link Assignator} as a whole.
 */
public class AssignatorIntegrationTest {

  private static final String NEWLINE = "\n";

  private MatchFormatter f = new MatchFormatter();

  @Test
  public void shouldProduceTrivialMatch() throws IOException {
    // given
    String events = "Drinking; D; 10; 2016-01-01 09:00 - 10:00";
    String attendants = "Peter; D";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is("Drinking: Peter"));
  }

  @Test
  public void shouldProduceReadmeExampleMatch() throws IOException {
    // given
    String events = "Chemistry; C; 2; 13:00 - 15:00" + NEWLINE +
                    "Biology; B; 2; 13:00 - 17:00" + NEWLINE +
                    "Geology; G; 2; 15:00 - 17:00";

    String attendants = "Alice; C, G, B" + NEWLINE +
                        "Bob; B, G" + NEWLINE +
                        "Claire; C" + NEWLINE +
                        "Doug; G, C" + NEWLINE +
                        "Eve; B";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is("Biology: Bob, Eve" + NEWLINE +
                                            "Chemistry: Alice, Claire" + NEWLINE +
                                            "Geology: Alice, Doug"));
  }

  @Test
  public void shouldNotUseSameSlotAgain() throws IOException {
    // given
    String events = "Event 1; E1; 10; 09:00 - 11:00" + NEWLINE +
                    "Event 2; E2; 10; 09:00 - 11:00" + NEWLINE +
                    "Event 3; E3; 10; 08:00 - 10:00, 11:15 - 13:00";


    String attendants = "Alice; E1, E2, E3" + NEWLINE +
                        "Bob; E1, E2, E3";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is("Event 1: Alice, Bob" + NEWLINE +
                                            "Event 2: " + NEWLINE +
                                            "Event 3: "));
  }


  @Test
  public void shouldNotPlaceAttendantInConflictingEvents() throws IOException {
    // given
    String events = "Event 1; E1; 10; 09:00 - 11:00" + NEWLINE +
                    "Event 2; E2; 10; 08:00 - 10:00";


    String attendants = "Alice; E1" + NEWLINE +
                        "Bob; E1" + NEWLINE +
                        "Claire; E1" + NEWLINE +
                        "Doug; E2, E1" + NEWLINE +
                        "Eve; E2";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is("Event 1: Alice, Bob, Claire" + NEWLINE +
                                            "Event 2: Doug, Eve"));
  }

  @Test
  public void shouldChooseOptimal() throws IOException {
    // given
    String events = "Event 1; E1; 10; 08:00 - 09:00" + NEWLINE +
        "Event 2; E2; 10; 09:00 - 10:00" + NEWLINE +
        "Event 3; E3; 10; 09:00 - 11:00";

    String attendants = "Alice; E2 | E1, E3";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is(
        "Event 1: Alice" + NEWLINE +
        "Event 2: " + NEWLINE +
        "Event 3: Alice"));
  }


  @Test
  public void shouldHandleAlternatives() throws IOException {
    // given
    String events = "Event 1; E1; 10; 08:00 - 09:00" + NEWLINE +
                    "Event 2; E2; 10; 09:00 - 10:00" + NEWLINE +
                    "Event 3; E3; 10; 09:00 - 11:00";


    String attendants = "Alice; E2 | E1, E3" + NEWLINE +
                        "Bob; E1 | E2, E3" + NEWLINE +
                        "Claire; E1 | E2 | E3" + NEWLINE +
                        "Doug; E1";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is("Event 1: Alice, Bob, Claire, Doug" + NEWLINE +
                                            "Event 2: " + NEWLINE +
                                            "Event 3: Alice, Bob"));
  }

  @Test
  public void shouldNotOverbook() throws IOException {
    // given
    String events = "Event 1; E1; 2; 08:00 - 09:00" + NEWLINE +
                    "Event 2; E2; 2; 09:00 - 10:00";


    String attendants = "Alice; E1 | E2" + NEWLINE +
                        "Bob; E1 | E2" + NEWLINE +
                        "Claire; E1" + NEWLINE +
                        "Doug; E1";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(bestMatch.score(), is(1.0));
    assertThat(f.formatShort(bestMatch), is("Event 1: Claire, Doug" + NEWLINE +
                                            "Event 2: Alice, Bob"));
  }

  @Test
  public void shouldChooseAlternative() throws IOException {
    // given
    String events = "Event 1; E1_1; 10; 2016-02-20 09:00 - 11:00" + NEWLINE +
                    "Event 1'; E1_2; 10; 2016-02-20 13:00 - 17:00" + NEWLINE +
                    "Event 2; E2; 10; 2016-02-20 08:00 - 10:00, 10:00 - 12:00";

    String attendants = "Doug; E2, E1_1 | E1_2";

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is(
        "Event 1: " + NEWLINE +
        "Event 1': Doug" + NEWLINE +
        "Event 2: Doug"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldDenyDuplicateEvents() {
    // given
    String events = "Event 1; E1; 10; 08:00 - 10:00";
    String attendants = "Alice; E1, E1";

    // when / then
    Assignator.findBestMatchFor(events, attendants);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldDenyDuplicateAlternatives() {
    // given
    String events = "Event 1; E1; 10; 08:00 - 10:00" + NEWLINE +
                    "Event 2; E2; 10; 10:00 - 12:00";
    String attendants = "Alice; E1, E2 | E1";

    // when / then
    Assignator.findBestMatchFor(events, attendants);
  }


  @Test
  public void shouldProcessExternalFile() throws Exception {
    // given
    File events = new File(urlOf("test1_events.txt"));
    File attendants = new File(urlOf("test1_attendants.txt"));

    // when
    Match bestMatch = Assignator.findBestMatchFor(events, attendants);

    // then
    assertThat(f.formatShort(bestMatch), is("Event 1 (1): Alice, Bob, Claire, Frank" + NEWLINE +
                                            "Event 1 (2): Doug" + NEWLINE +
                                            "Event 2: Doug, Eve"));
  }

  private URI urlOf(String path) throws URISyntaxException {
    return getClass().getClassLoader().getResource(path).toURI();
  }
}
