package ch.retorte.assignator.event;

import org.junit.Test;

import static ch.retorte.assignator.AssignatorTestTools.ts;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link TimeSlot}.
 */
public class TimeSlotTest {

  @Test
  public void shouldDetermineIntersection() {
    assertTrue(intersecting("09 - 11", "10 - 12"));
    assertTrue(intersecting("10 - 12", "09 - 11"));
    assertTrue(intersecting("09 - 12", "10 - 11"));
    assertTrue(intersecting("09 - 10", "09 - 12"));

    assertFalse(intersecting("09 - 10", "11 - 12"));
    assertFalse(intersecting("09 - 10", "10 - 11"));
  }

  private boolean intersecting(String timeSlot1, String timeSlot2) {
    return ts(timeSlot1).doesIntersectWith(ts(timeSlot2));
  }
}
