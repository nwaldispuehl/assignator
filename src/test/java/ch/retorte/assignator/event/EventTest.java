package ch.retorte.assignator.event;

import org.junit.Test;

import static ch.retorte.assignator.AssignatorTestTools.e;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link Event}.
 */
public class EventTest {

  private Event e1 = e("e1", 1, "08 - 10");
  private Event e2 = e("e2", 1, "10 - 12");
  private Event e3 = e("e3", 1, "09 - 11");



  @Test
  public void shouldDetermineBasicIntersections() {
    assertTrue(e1.doesIntersectWith(e3));
    assertTrue(e2.doesIntersectWith(e3));

    assertFalse(e1.doesIntersectWith(e2));
  }

  private Event e4 = e("e4", 1, "08 - 09", "10 - 11", "13 - 17");
  private Event e5 = e("e5", 1, "08 - 12", "13 - 18");
  private Event e6 = e("e6", 1, "09 - 10", "12 - 13");
  private Event e7 = e("e7", 1, "07 - 20");
  private Event e8 = e("e8", 1, "04 - 05", "21 - 22");

  @Test
  public void shouldDetermineAdvancedIntersections() {
    assertTrue(e4.doesIntersectWith(e5));
    assertTrue(e5.doesIntersectWith(e4));

    assertFalse(e4.doesIntersectWith(e6));
    assertFalse(e6.doesIntersectWith(e4));

    assertTrue(e4.doesIntersectWith(e7));
    assertTrue(e7.doesIntersectWith(e4));

    assertFalse(e4.doesIntersectWith(e8));
    assertFalse(e8.doesIntersectWith(e4));

    assertTrue(e5.doesIntersectWith(e6));
    assertTrue(e6.doesIntersectWith(e5));

    assertTrue(e5.doesIntersectWith(e7));
    assertTrue(e7.doesIntersectWith(e5));

    assertFalse(e5.doesIntersectWith(e8));
    assertFalse(e8.doesIntersectWith(e5));

    assertTrue(e6.doesIntersectWith(e7));
    assertTrue(e7.doesIntersectWith(e6));

    assertFalse(e7.doesIntersectWith(e8));
    assertFalse(e8.doesIntersectWith(e7));
  }

}
