package ch.retorte.assignator.parser;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit tests for the {@link TextParser}.
 */
public class TextParserTest {

  private TextParser sut = new TextParser();

  @Test
  public void shouldSplitByLastOccurrenceOfDelimiter() {
    // given
    String input = "A - B - C - D";
    String delimiter = "-";

    // when
    List<String> result = sut.splitAtLastOccurrenceWith(input, delimiter);

    // then
    assertThat(result.get(1), is(" D"));
  }

  @Test
  public void shouldNotSplitByLastOccurrenceOfDelimiterIfNonePresent() {
    // given
    String input = "A, B, C, D";
    String delimiter = "-";

    // when
    List<String> result = sut.splitAtLastOccurrenceWith(input, delimiter);

    // then
    assertThat(result.size(), is(1));
    assertThat(result.get(0), is(input));
  }
}
