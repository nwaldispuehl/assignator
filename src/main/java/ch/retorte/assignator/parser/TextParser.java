package ch.retorte.assignator.parser;

import ch.retorte.assignator.matcher.MatcherInput;
import ch.retorte.assignator.matcher.Matcher;
import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.attendant.Preference;
import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.event.TimeSlot;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * Interface for parsers providing data to the {@link Matcher}.
 */
public class TextParser {

  //---- Static

  private static final String FIELD_DELIMITER = ";";
  private static final String SUB_FIELD_DELIMITER = ",";
  private static final String ALTERNATIVES_DELIMITER = "\\|";
  private static final String RANGE_DELIMITER = "-";
  private static final String SECOND_SUFFIX = ":00";

  private Map<String, Event> events = new HashMap<>();
  private List<Attendant> attendants = new ArrayList<>();

  public MatcherInput parse(String eventsString, String attendantsString) {
    parseEventsFrom(eventsString);
    parseAttendantsFrom(attendantsString);
    return createResult();
  }

  private MatcherInput createResult() {
    return new MatcherInput(new ArrayList<>(events.values()), attendants);
  }

  private void parseEventsFrom(String eventsString) {
    List<String> eventLines = getRelevantLinesFrom(eventsString);
    for (String line : eventLines) {
      Event event = parseEventFrom(line);
      if (event != null) {
        events.put(event.getId(), event);
      }
    }
  }

  private void parseAttendantsFrom(String attendantsString) {
    List<String> attendantsLine = getRelevantLinesFrom(attendantsString);
    for (String line : attendantsLine) {
      Attendant attendant = parseAttendantFrom(line);
      if (attendant != null) {
        attendants.add(attendant);
      }
    }
  }

  private List<String> getRelevantLinesFrom(String text) {
    return Arrays.stream(text.split(System.lineSeparator()))
        .filter(this::isUsable)
        .collect(Collectors.toList());
  }

  private boolean isUsable(String line) {
    return line != null && !line.trim().isEmpty() && !line.trim().startsWith("#");
  }

  private Event parseEventFrom(String line) {
    List<String> fieldTokens = splitWith(line, FIELD_DELIMITER);
    if (fieldTokens.size() == 4) {
      String name = fieldTokens.get(0).trim();
      String id = fieldTokens.get(1).trim();
      int capacity = Integer.valueOf(fieldTokens.get(2).trim());
      List<TimeSlot> timeSlots = parseTimeSlotsFrom(fieldTokens.get(3));

      if (!timeSlots.isEmpty()) {
        return new Event(name, id, capacity, timeSlots);
      }
      else {
        throw new IllegalArgumentException("Events must have at least one time slot. Check entry '" + name + "'.");
      }
    }

    return null;
  }

  private List<String> splitWith(String input, String delimiter) {
    return splitWith(input, delimiter, 0);
  }

  private List<String> splitWith(String input, String delimiter, int limit) {
    return asList(input.split(delimiter, limit));
  }

  private List<TimeSlot> parseTimeSlotsFrom(String timeSlotsLine) {
    List<TimeSlot> result = new ArrayList<>();
    List<String> timeSlotTokens = splitWith(timeSlotsLine, SUB_FIELD_DELIMITER);
    LocalDate lastDate = null;
    for (String timeSlotToken : timeSlotTokens) {
      TimeSlot timeSlot = parseTimeSlotFrom(timeSlotToken, lastDate);
      if (timeSlot != null) {
        result.add(timeSlot);
        lastDate = timeSlot.getStart().toLocalDate();
      }
    }

    return result;
  }

  private TimeSlot parseTimeSlotFrom(String token, LocalDate lastDate) {
    List<String> rangeParts = splitAtLastOccurrenceWith(token, RANGE_DELIMITER);
    if (rangeParts.size() == 2) {

      String from = rangeParts.get(0).trim();
      String to = rangeParts.get(1).trim();

      if (hasDate(from)) {
        List<String> fromParts = splitWith(from, " ", 2);

        String datePart = fromParts.get(0);
        lastDate = LocalDate.parse(datePart);
        from = fromParts.get(1);
      }
      else {
        if (lastDate == null) {
          lastDate = LocalDate.now();
        }
      }

      LocalTime fromTime = LocalTime.parse(from + SECOND_SUFFIX);
      LocalTime toTime = LocalTime.parse(to + SECOND_SUFFIX);

      return new TimeSlot(LocalDateTime.of(lastDate, fromTime), LocalDateTime.of(lastDate, toTime));
    }
    return null;
  }

  List<String> splitAtLastOccurrenceWith(String input, String delimiter) {
    if (!input.contains(delimiter)) {
      return singletonList(input);
    }

    int lastIndex = input.lastIndexOf(delimiter);
    return asList(input.substring(0, lastIndex), input.substring(lastIndex + 1, input.length()));
  }

  private boolean hasDate(String timeSlotString) {
    return 5 < timeSlotString.length();
  }

  private Attendant parseAttendantFrom(String line) {
    List<String> fieldTokens = splitWith(line, FIELD_DELIMITER);
    if (fieldTokens.size() == 2) {
      String name = fieldTokens.get(0).trim();
      List<Preference> preferences = parsePreferencesFrom(fieldTokens.get(1));

      if (!preferences.isEmpty()) {
        return new Attendant(name, preferences);
      }
      else {
        throw new IllegalArgumentException("Attendants must have at least one valid preference. Check entry '" + name + "'.");
      }
    }

    return null;
  }

  private List<Preference> parsePreferencesFrom(String preferencesLine) {
    List<String> preferenceTokens = splitWith(preferencesLine, SUB_FIELD_DELIMITER);
    List<Preference> result = new ArrayList<>();
    for (String preferenceToken : preferenceTokens) {
      Preference preference = new Preference();

      List<String> alternatives = splitWith(preferenceToken, ALTERNATIVES_DELIMITER);
      for (String eventId : alternatives) {
        Event event = events.get(eventId.trim());
        if (event != null) {
          preference.add(event);
        }
      }

      if (preference.hasEvents()) {
        result.add(preference);
      }
    }
    return result;
  }

}
