package ch.retorte.assignator.matcher;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.match.EventAssignment;
import ch.retorte.assignator.match.Match;

import java.util.*;

/**
 * The {@link RandomizedGreedyMatcher} assigns the first match and then shuffles the attendants list.
 */
public class RandomizedGreedyMatcher implements Matcher {

  //---- Static

  /* There are attendants * MULTIPLIER randomized rounds. */
  private static final int MULTIPLIER = 3;


  //---- Methods

  @Override
  public List<Match> findMatchesBetween(List<Event> events, List<Attendant> attendants) {
    List<Match> result = new ArrayList<>();

    for (int i = 0; i < attendants.size() * MULTIPLIER; i++) {
      result.add(findMatchBetween(events, attendants));
      randomize(attendants);
    }

    return result;
  }

  private Match findMatchBetween(List<Event> events, List<Attendant> attendants) {
    Map<Event, EventAssignment> eventAssignments = prepareEventAssignmentMapWith(events);

    List<Attendant> activeAttendants = new ArrayList<>(attendants);
    while (!activeAttendants.isEmpty()) {
      List<Attendant> inspectedAttendants = new ArrayList<>(activeAttendants);
      for (Attendant a : inspectedAttendants) {
        Event highestRankedEvent = a.findHighestRankedEventIn(eventAssignments.values());
        if (highestRankedEvent != null) {
          eventAssignments.get(highestRankedEvent).add(a);
        }
        else {
          activeAttendants.remove(a);
        }
      }
    }

    return new Match(attendants, eventAssignments.values());
  }

  private Map<Event, EventAssignment> prepareEventAssignmentMapWith(List<Event> events) {
    Map<Event, EventAssignment> result = new HashMap<>();
    events.forEach(event -> result.put(event, new EventAssignment(event)));
    return result;
  }

  private void randomize(List<Attendant> attendants) {
    Collections.shuffle(attendants);
  }
}
