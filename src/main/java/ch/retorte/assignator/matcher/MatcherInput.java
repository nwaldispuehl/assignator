package ch.retorte.assignator.matcher;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.event.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds all input data used by the {@link Matcher}.
 */
public class MatcherInput {

  private List<Event> events = new ArrayList<>();
  private List<Attendant> attendants = new ArrayList<>();

  public MatcherInput(List<Event> events, List<Attendant> attendants) {
    this.events = events;
    this.attendants = attendants;
  }

  public List<Event> getEvents() {
    return events;
  }

  public List<Attendant> getAttendants() {
    return attendants;
  }
}
