package ch.retorte.assignator.matcher;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.match.Match;

import java.util.List;

/**
 * The {@link Matcher} finds matches between a number of attendants with preferences and a number of events.
 */
public interface Matcher {

  //---- Methods

  List<Match> findMatchesBetween(List<Event> events, List<Attendant> attendants);
}
