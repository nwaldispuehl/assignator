package ch.retorte.assignator;

import ch.retorte.assignator.formatter.MatchFormatter;
import ch.retorte.assignator.match.Match;
import ch.retorte.assignator.matcher.Matcher;
import ch.retorte.assignator.matcher.MatcherInput;
import ch.retorte.assignator.matcher.RandomizedGreedyMatcher;
import ch.retorte.assignator.parser.TextParser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * Command line starter of the {@link Matcher}.
 */
public class Assignator {

  //---- Main method

  public static void main(String[] args) throws IOException {
    if (args.length != 2) {
      System.err.println("Assignator. Software to calculate most interesting person to event assignment. Usage:");
      System.err.println("  assignator EVENTS_FILE ATTENDANTS_FILE");
      System.err.println();
      return;
    }

    String eventsFileName = args[0];
    String attendantsFileName = args[1];

    Match match = findBestMatchFor(new File(eventsFileName), new File(attendantsFileName));

    printFormatted(match);
    printScoreOf(match);
  }


  //---- Methods

  static Match findBestMatchFor(File eventsFile, File attendantsFile) throws IOException {
    return findBestMatchWithErrorHandlingFor(getContentOf(eventsFile), getContentOf(attendantsFile));
  }

  private static String getContentOf(File file) throws IOException {
    return new String(Files.readAllBytes(file.toPath()));
  }

  private static Match findBestMatchWithErrorHandlingFor(String eventsText, String attendantsText) {
    try {
      return findBestMatchFor(eventsText, attendantsText);
    }
    catch (Exception e) {
      System.err.println("ERROR " + e.getMessage());
      System.err.println();

      System.exit(1);
      return null;
    }
  }

  static Match findBestMatchFor(String eventsText, String attendantsText) {
      MatcherInput matcherInput = new TextParser().parse(eventsText, attendantsText);
      List<Match> matches = new RandomizedGreedyMatcher().findMatchesBetween(matcherInput.getEvents(), matcherInput.getAttendants());
      return bestOf(matches);
  }

  private static Match bestOf(List<Match> matches) {
    Match result = null;
    double score = -1;

    for (Match m : matches) {
      if (score <= m.score()) {
        result = m;
        score = m.score();
      }
    }

    return result;
  }

  private static void printFormatted(Match match) {
    MatchFormatter matchFormatter = new MatchFormatter();

    System.out.println(matchFormatter.formatByEvents(match));
    System.out.println(matchFormatter.formatByAttendants(match));
  }


  private static void printScoreOf(Match match) {
    System.out.println("Best match score: " + match.score());
  }


}
