package ch.retorte.assignator.attendant;

import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.match.EventAssignment;
import ch.retorte.assignator.match.EventAssignmentGroup;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

/**
 * Represents a person/entity trying to attend a number of preferred events.
 */
public class Attendant {

  //---- Fields

  private final String name;
  private final List<Preference> preferences = new ArrayList<>();


  //---- Constructor

  Attendant() {
    name = "";
  }

  public Attendant(String name, Preference... preferences) {
    this(name, asList(preferences));
  }

  public Attendant (String name, List<Preference> preferences) {
    if (preferences.isEmpty()) {
      throw new IllegalArgumentException("Attendant '" + name + "': Must have at least one preferred event.");
    }
    if (hasDuplicateEvents(preferences)) {
      throw new IllegalArgumentException("Attendant '" + name + "': Each event must only occur once in the preferences.");
    }

    this.name = name;
    this.preferences.addAll(preferences);
  }


  //---- Methods

  private boolean hasDuplicateEvents(List<Preference> preferences) {
    Set<Event> eventIds = new HashSet<>();
    for (Preference p : preferences) {
      for (Event e : p.getAlternatives()) {
        if (eventIds.contains(e)) {
          return true;
        }
        else {
          eventIds.add(e);
        }
      }
    }
    return false;
  }

  public String getName() {
    return name;
  }

  private List<Preference> getPreferences() {
    return preferences;
  }

  public Event findHighestRankedEventIn(Collection<EventAssignment> eventAssignments) {
    return findHighestRankedEventFor(this, preferences, new EventAssignmentGroup(eventAssignments));
  }

  static Event findHighestRankedEventFor(Attendant attendant, List<Preference> preferences, EventAssignmentGroup eventAssignmentGroup) {
    List<Preference> preferencesWithCapacityLeft = getStillAvailablePreferences(attendant, preferences, eventAssignmentGroup);
    return highestAvailableEventIn(attendant, preferencesWithCapacityLeft, preferences, eventAssignmentGroup);
  }

  private static Event highestAvailableEventIn(Attendant attendant, List<Preference> preferencesWithCapacityLeft, List<Preference> preferences, EventAssignmentGroup eventAssignmentGroup) {
    return determineMostUsefulAlternativeOf(attendant, new ArrayList<>(), preferencesWithCapacityLeft, preferences, eventAssignmentGroup);
  }

  private static Event determineMostUsefulAlternativeOf(Attendant attendant, List<Event> preferencesSoFar, List<Preference> remainingPreferences, List<Preference> preferences, EventAssignmentGroup eventAssignmentGroup) {
    List<Event> bestEvents = findBestEventWith(attendant, preferencesSoFar, remainingPreferences, preferences, eventAssignmentGroup);
    if (bestEvents.isEmpty()) {
      return null;
    }
    else {
      return bestEvents.get(0);
    }
  }

  private static List<Event> findBestEventWith(Attendant attendant, List<Event> preferencesSoFar, List<Preference> remainingPreferences, List<Preference> preferences, EventAssignmentGroup eventAssignmentGroup) {
    if (remainingPreferences.isEmpty()) {
      return preferencesSoFar;
    }

    Preference nextPreference = remainingPreferences.get(0);

      List<List<Event>> results = new ArrayList<>();
      for (Event e : nextPreference.getAlternatives()) {

        if (isUsableWith(attendant, e, preferencesSoFar, eventAssignmentGroup)) {
          results.add(findBestEventWith(attendant, append(preferencesSoFar, e), remove(nextPreference, remainingPreferences), preferences, eventAssignmentGroup));
        }
        else {
          results.add(findBestEventWith(attendant, preferencesSoFar, remove(nextPreference, remainingPreferences), preferences, eventAssignmentGroup));
        }
      }

      return highestRankedListIn(results, preferences);
  }


  private static List<Event> highestRankedListIn(List<List<Event>> listOfEventLists, List<Preference> preferences) {
    double score = -1;
    List<Event> result = null;

    for (List<Event> events : listOfEventLists) {
      double newScore = calculateScoreWith(preferences, events);
      if (score < newScore) {
        score = newScore;
        result = events;
      }
    }

    return result;
  }

  private static List<Event> append(List<Event> events, Event event) {
    List<Event> result = new ArrayList<>(events);
    result.add(event);
    return result;
  }

  private static List<Preference> remove(Preference preference, List<Preference> preferences) {
    List<Preference> result = new ArrayList<>(preferences);
    result.remove(preference);
    return result;
  }

  private static boolean isUsableWith(Attendant attendant, Event event, List<Event> preferencesSoFar, EventAssignmentGroup eventAssignmentGroup) {
    return eventAssignmentGroup.getFor(event).hasCapacity()
        && !eventAssignmentGroup.getFor(event).has(attendant)
        && !event.doesIntersectWithAnyOf(preferencesSoFar)
        && !event.doesIntersectWithAnyOf(eventAssignmentGroup.getAssignedEventsFor(attendant));
  }

  private static List<Preference> getStillAvailablePreferences(Attendant attendant, List<Preference> preferences, EventAssignmentGroup eventAssignmentGroup) {
    return preferences
        .stream()
        .filter(
          preference -> isAvailable(attendant, preference, eventAssignmentGroup)
        ).collect(Collectors.toList());
  }

  private static boolean isAvailable(Attendant attendant, Preference preference, EventAssignmentGroup eventAssignmentGroup) {
    for (Event e : preference.getAlternatives()) {
      if (eventAssignmentGroup.hasFor(e) && eventAssignmentGroup.getFor(e).has(attendant)) {
        return false;
      }
    }

    return preference.hasCapacityIn(eventAssignmentGroup);
  }

  List<Event> sortByRank(List<Event> highestRankedEvents) {
    Collections.sort(highestRankedEvents, (e1, e2) -> getRankWith(preferences, e1) - getRankWith(preferences, e2));
    return highestRankedEvents;
  }

  public double calculateScoreFor(EventAssignmentGroup eventAssignmentGroup) {
    return calculateScoreWith(this, preferences, eventAssignmentGroup);
  }

  double calculateScoreWith(Attendant attendant, List<Preference> preferences, EventAssignmentGroup eventAssignmentGroup) {

    List<Event> chosenEvents = eventAssignmentGroup.getEventAssignments()
        .stream()
        .filter(eventAssignment -> eventAssignment.has(attendant))
        .map(EventAssignment::getEvent)
        .collect(Collectors.toList());

    return calculateScoreWith(preferences, chosenEvents);
  }

  private static double calculateScoreWith(List<Preference> preferences, Collection<Event> chosenEvents) {
    int preferenceCount = preferences.size();
    double scorePerEvent = 1.0 / sumUpTo(preferenceCount);
    double result = 0;

    for (Preference p : preferences) {
      for (Event e : p.getAlternatives()) {
        if (chosenEvents.contains(e)) {
          double referenceScore = preferenceCount - getRankWith(preferences, e);
          result = result + scorePerEvent * referenceScore;
          break;
        }
      }

    }
    return result;
  }

  private static int sumUpTo(int n) {
    return n * (n + 1) / 2;
  }

  private static int getRankWith(List<Preference> preferences, Event event) {
    if (preferences.isEmpty()) {
      return 0;
    }

    for (Preference preference : preferences) {
      if (preference.contains(event)) {
        return preferences.indexOf(preference);
      }
    }

    return 0;
  }

  @Override
  public String toString() {
    return name + ", Preferences: " + getPreferencesList();
  }

  private String getPreferencesList() {
    StringJoiner preferenceJoiner = new StringJoiner(", ");
    for (Preference p: getPreferences()) {
      StringJoiner alternativesJoiner = new StringJoiner(" | ");

      for (Event e : p.getAlternatives()) {
        alternativesJoiner.add(e.getId());
      }

      preferenceJoiner.add(alternativesJoiner.toString());
    }
    return preferenceJoiner.toString();
  }
}
