package ch.retorte.assignator.attendant;

import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.match.EventAssignmentGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a single event preference.
 */
public class Preference {

  //---- Fields

  private List<Event> alternatives = new ArrayList<>();


  //---- Constructors

  public Preference() {}

  public Preference(Event event) {
    add(event);
  }


  //---- Methods

  public void add(Event event) {
    alternatives.add(event);
//    Collections.shuffle(alternatives);
  }

  public boolean hasEvents() {
    return !alternatives.isEmpty();
  }

  boolean contains(Event event) {
    return alternatives.contains(event);
  }

  List<Event> getAlternatives() {
    return alternatives;
  }

  boolean hasSingleAlternative() {
    return alternatives.size() == 1;
  }

  Event getFirst() {
    return alternatives.get(0);
  }

  boolean hasCapacityIn(EventAssignmentGroup eventAssignmentGroup) {
    for (Event e : alternatives) {
      if (eventAssignmentGroup.hasFor(e) && eventAssignmentGroup.getFor(e).hasCapacity()) {
        return true;
      }
    }
    return false;
  }

  boolean doesIntersectWithAnyOf(List<Event> events) {
    for (Event e : alternatives) {
      if (e.doesIntersectWithAnyOf(events)) {
        return true;
      }
    }
    return false;
  }
}
