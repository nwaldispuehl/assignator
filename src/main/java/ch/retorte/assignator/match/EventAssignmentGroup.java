package ch.retorte.assignator.match;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.event.Event;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Groups a list of {@link EventAssignment} to a convenient bundle.
 */
public class EventAssignmentGroup {

  //---- Fields

  private Map<Event, EventAssignment> eventAssignmentMap = new HashMap<>();


  //---- Constructor

  public EventAssignmentGroup(Collection<EventAssignment> eventAssignments) {
    eventAssignments.forEach(eventAssignment -> eventAssignmentMap.put(eventAssignment.getEvent(), eventAssignment));
  }

  public EventAssignment getFor(Event event) {
    return eventAssignmentMap.get(event);
  }

  public Collection<EventAssignment> getEventAssignments() {
    return eventAssignmentMap.values();
  }

  public List<Event> getAssignedEventsFor(Attendant attendant) {
    return eventAssignmentMap.values()
        .stream()
        .filter(eventAssignment -> eventAssignment.has(attendant))
        .map(EventAssignment::getEvent)
        .collect(Collectors.toList());
  }

  public boolean hasFor(Event event) {
    return eventAssignmentMap.containsKey(event);
  }
}
