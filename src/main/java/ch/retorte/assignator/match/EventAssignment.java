package ch.retorte.assignator.match;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.event.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Assigns multiple {@link Attendant} to an {@link Event}.
 */
public class EventAssignment {

  //---- Fields

  private Event event;
  private List<Attendant> attendants = new ArrayList<>();


  //---- Constructor

  public EventAssignment(Event event) {
    this.event = event;
  }


  //---- Methods

  public boolean hasCapacity() {
    return attendants.size() < event.getCapacity();
  }

  public EventAssignment add(Attendant attendant) {
    if (has(attendant)) {
      throw new IllegalStateException("Attendant (" + attendant.getName() + ") must only attend an event (" + event.getName() + ") once.");
    }

    if (!hasCapacity()) {
      throw new IllegalStateException("Event (" + event.getName() + ") must not be overbooked.");
    }
    attendants.add(attendant);
    return this;
  }

  public Event getEvent() {
    return event;
  }

  public List<Attendant> getAttendants() {
    return attendants;
  }

  public boolean has(Attendant attendant) {
    return attendants.contains(attendant);
  }
}
