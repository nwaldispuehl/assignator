package ch.retorte.assignator.match;


import ch.retorte.assignator.matcher.Matcher;
import ch.retorte.assignator.attendant.Attendant;

import java.util.Collection;

/**
 * One possible outcome of the assignment process of the {@link Matcher}. Each {@link Match} is scored,
 * which should be a measure of how complete the preferences of the attendees are met.
 */
public class Match {

  //---- Fields

  private double score = 0;
  private final Collection<Attendant> attendants;
  private final EventAssignmentGroup eventAssignmentGroup;


  //---- Constructor

  public Match(Collection<Attendant> attendants, Collection<EventAssignment> eventAssignments) {
    this.attendants = attendants;
    this.eventAssignmentGroup = new EventAssignmentGroup(eventAssignments);

    calculateScore();
  }


  //---- Methods

  private void calculateScore() {
    score = calculateScoreOf(attendants, eventAssignmentGroup);
  }

  private double calculateScoreOf(Collection<Attendant> attendants, EventAssignmentGroup eventAssignments) {
    double result = 0;

    for(Attendant a : attendants) {
      result = result + a.calculateScoreFor(eventAssignments);
    }

    return result / attendants.size();
  }

  public double score() {
    return score;
  }

  public EventAssignmentGroup getEventAssignmentGroup() {
    return eventAssignmentGroup;
  }

  public Collection<Attendant> getAttendants() {
    return attendants;
  }
}
