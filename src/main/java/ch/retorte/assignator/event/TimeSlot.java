package ch.retorte.assignator.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents a time slot an {@link Event} takes place in. An {@link Event} might consist of multiple time slots.
 */
public class TimeSlot {

  //---- Static

  private static final DateTimeFormatter FULL_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
  private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

  //---- Fields

  private final LocalDateTime start;
  private final LocalDateTime end;


  //---- Constructor

  public TimeSlot(LocalDateTime start, LocalDateTime end) {
    if (start.isAfter(end)) {
      throw new IllegalArgumentException("End time must be after start time.");
    }

    this.start = start;
    this.end = end;
  }

  //---- Methods

  boolean doesIntersectWith(TimeSlot otherTimeSlot) {
    return !isBefore(otherTimeSlot) && !isAfter(otherTimeSlot);
  }

  private boolean isBefore(TimeSlot otherTimeSlot) {
    return end.isBefore(otherTimeSlot.start) || end.isEqual(otherTimeSlot.start);
  }

  private boolean isAfter(TimeSlot otherTimeSlot) {
    return start.isAfter(otherTimeSlot.end) || start.isEqual(otherTimeSlot.end);
  }

  int compareTo(TimeSlot timeSlot) {
    return start.compareTo(timeSlot.start);
  }

  public LocalDateTime getStart() {
    return start;
  }

  @Override
  public String toString() {
    return start.format(FULL_FORMATTER) + " - " + end.format(TIME_FORMATTER);
  }
}
