package ch.retorte.assignator.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * A single event, defined by an id, a maximal number of people attending it and a number of one or more time slots.
 */
public class Event {

  //---- Fields

  private String name;
  private final String id;
  private final int capacity;
  private final List<TimeSlot> timeSlots = new ArrayList<>();


  //---- Constructor

  public Event(String id, int capacity, TimeSlot... timeSlots) {
    this(id, id, capacity, timeSlots);
  }

  public Event(String name, String id, int capacity, TimeSlot... timeSlots) {
    this(name, id, capacity, asList(timeSlots));
  }

  public Event(String name, String id, int capacity, List<TimeSlot> timeSlots) {
    this.name = name;
    this.id = id;
    this.capacity = capacity;
    this.timeSlots.addAll(sort(timeSlots));
  }


  //---- Methods


  public String getName() {
    return name;
  }

  public String getId() {
    return id;
  }

  public int getCapacity() {
    return capacity;
  }

  public boolean doesIntersectWithAnyOf(Collection<Event> eventList) {
    for (Event e : eventList) {
      if (this != e && this.doesIntersectWith(e)) {
        return true;
      }
    }
    return false;
  }

  boolean doesIntersectWith(Event otherEvent) {
    for (TimeSlot thisTimeSlot : timeSlots) {
      for (TimeSlot otherTimeSlot : otherEvent.timeSlots) {
        if (thisTimeSlot.doesIntersectWith(otherTimeSlot)) {
          return true;
        }
      }
    }
    return false;
  }

  private List<TimeSlot> sort(Collection<TimeSlot> timeSlots) {
    List<TimeSlot> result = new ArrayList<>(timeSlots);
    Collections.sort(result, TimeSlot::compareTo);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof Event ? id.equals(((Event) obj).id) : super.equals(obj);
  }

  @Override
  public String toString() {
    return name + " (id: '" + id + "', capacity: " + capacity + ") time slot(s): " + timeSlots;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
