package ch.retorte.assignator.formatter;

import ch.retorte.assignator.attendant.Attendant;
import ch.retorte.assignator.event.Event;
import ch.retorte.assignator.match.EventAssignment;
import ch.retorte.assignator.match.Match;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Produces nice textual representations of matches.
 */
public class MatchFormatter {

  //---- Static

  private static final String INDENTATION = "  ";
  private static final String TOP_LEVEL_ENUMERATOR = "* ";
  private static final String SECOND_LEVEL_ENUMERATOR = "- ";
  private static final String NEWLINE = System.lineSeparator();


  //---- Fields

  private DecimalFormat decimalFormat = new DecimalFormat("#.##");


  //---- Methods

  public String formatShort(Match match) {
    StringJoiner eventJoiner = new StringJoiner(NEWLINE);

    for (EventAssignment eventAssignment : getSortedEventAssignmentsFrom(match)) {
      String event = eventAssignment.getEvent().getName();
      StringJoiner attendantJoiner = new StringJoiner(", ");
      for (Attendant attendant : getSortedAttendantsFrom(eventAssignment.getAttendants())) {
        attendantJoiner.add(attendant.getName());
      }
      event = event + ": " + attendantJoiner.toString();
      eventJoiner.add(event);
    }
    return eventJoiner.toString();
  }

  public String formatByEvents(Match match) {
    String result = "Match: events: " + match.getEventAssignmentGroup().getEventAssignments().size() + ", assignees: " + match.getAttendants().size() + ", score: " + decimalFormat.format(match.score()) + ", attendants:" + NEWLINE;

    for (Attendant attendant : getSortedAttendantsFrom(match.getAttendants())) {
      double attendantScore = attendant.calculateScoreFor(match.getEventAssignmentGroup());
      result = result + NEWLINE + TOP_LEVEL_ENUMERATOR + attendant + ", hit score: " + decimalFormat.format(attendantScore) + ", events (" + match.getEventAssignmentGroup().getAssignedEventsFor(attendant).size() + "):" + NEWLINE;

      for (Event event : getSortedEventsFrom(match.getEventAssignmentGroup().getAssignedEventsFor(attendant))) {
        result = result + INDENTATION + SECOND_LEVEL_ENUMERATOR + event + NEWLINE;
      }

    }

    return result;
  }

  public String formatByAttendants(Match match) {
    String result = "Match: events: " + match.getEventAssignmentGroup().getEventAssignments().size() + ", assignees: " + match.getAttendants().size() + ", score: " + decimalFormat.format(match.score()) + ", assignments:" + NEWLINE;

    for (EventAssignment eventAssignment : getSortedEventAssignmentsFrom(match)) {
      result = result + NEWLINE + TOP_LEVEL_ENUMERATOR + eventAssignment.getEvent() + ", attendants (" + eventAssignment.getAttendants().size() + "):" + NEWLINE;

      for (Attendant attendant : getSortedAttendantsFrom(eventAssignment.getAttendants())) {
        result = result + INDENTATION + SECOND_LEVEL_ENUMERATOR + attendant.getName() + NEWLINE;
      }

    }

    return result;
  }

  private List<EventAssignment> getSortedEventAssignmentsFrom(Match match) {
    List<EventAssignment> result = new ArrayList<>(match.getEventAssignmentGroup().getEventAssignments());
    Collections.sort(result, (o1, o2) -> o1.getEvent().getName().compareTo(o2.getEvent().getName()));
    return result;
  }

  private List<Attendant> getSortedAttendantsFrom(Collection<Attendant> attendants) {
    List<Attendant> result = new ArrayList<>(attendants);
    Collections.sort(result, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    return result;
  }

  private List<Event> getSortedEventsFrom(Collection<Event> events) {
    List<Event> result = new ArrayList<>(events);
    Collections.sort(result, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    return result;
  }

}
